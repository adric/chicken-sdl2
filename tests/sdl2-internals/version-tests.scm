
(test-begin "version")


(test-group "sdl2:make-version"
  (test-assert (sdl2:version? (sdl2:make-version)))
  (test 0 (sdl2:version-major (sdl2:make-version)))
  (test 0 (sdl2:version-minor (sdl2:make-version)))
  (test 0 (sdl2:version-patch (sdl2:make-version)))

  (test-assert (sdl2:version? (sdl2:make-version 1)))
  (test 1 (sdl2:version-major (sdl2:make-version 1)))
  (test 0 (sdl2:version-minor (sdl2:make-version 1)))
  (test 0 (sdl2:version-patch (sdl2:make-version 1)))

  (test-assert (sdl2:version? (sdl2:make-version 1 2 3)))
  (test 1 (sdl2:version-major (sdl2:make-version 1 2 3)))
  (test 2 (sdl2:version-minor (sdl2:make-version 1 2 3)))
  (test 3 (sdl2:version-patch (sdl2:make-version 1 2 3))))


(test-group "sdl2:version?"
  (test-assert (sdl2:version? (sdl2:make-version)))
  (test-assert (sdl2:version? (sdl2:make-version 1 2 3)))
  (test-assert (not (sdl2:version? '(1 2 3))))
  (test-assert (not (sdl2:version? #(1 2 3))))
  (test-assert (not (sdl2:version? (sdl2:make-point)))))


(test-integer-struct-fields
 make: (sdl2:make-version)
 freer: sdl2:free-version!
 (major
  getter: sdl2:version-major
  setter: sdl2:version-major-set!
  min: Uint8-min
  max: Uint8-max)
 (minor
  getter: sdl2:version-minor
  setter: sdl2:version-minor-set!
  min: Uint8-min
  max: Uint8-max)
 (patch
  getter: sdl2:version-patch
  setter: sdl2:version-patch-set!
  min: Uint8-min
  max: Uint8-max))


(test-group "sdl2:version-set!"
  (let ((version (sdl2:make-version)))
    (test-assert "returns the version"
                 (eq? version (sdl2:version-set! version 5 6 7))))
  (test "sets all fields if all values are specified"
        '(5 6 7)
        (sdl2:version->list
         (sdl2:version-set! (sdl2:make-version 1 2 3) 5 6 7)))
  (test "does not change fields where the value is omitted"
        '(5 6 3)
        (sdl2:version->list
         (sdl2:version-set! (sdl2:make-version 1 2 3) 5 6)))
  (test "has no effect if all values are omitted"
        '(1 2 3)
        (sdl2:version->list
         (sdl2:version-set! (sdl2:make-version 1 2 3))))
  (test "does not change fields where the value is #f"
        '(1 2 8)
        (sdl2:version->list
         (sdl2:version-set! (sdl2:make-version 1 2 3) #f #f 8)))
  (test "has no effect if all values are #f"
        '(1 2 3)
        (sdl2:version->list
         (sdl2:version-set! (sdl2:make-version 1 2 3) #f #f #f))))


(test-group "sdl2:alloc-version"
  (let ((version (sdl2:alloc-version)))
    (test-assert (sdl2:version? version))
    (test-assert (integer? (sdl2:version-major version)))
    (test-assert (integer? (sdl2:version-minor version)))
    (test-assert (integer? (sdl2:version-patch version)))))

(test-group "sdl2:alloc-version*"
  (let ((version (sdl2:alloc-version*)))
    (test-assert (sdl2:version? version))
    (test-assert (integer? (sdl2:version-major version)))
    (test-assert (integer? (sdl2:version-minor version)))
    (test-assert (integer? (sdl2:version-patch version)))
    (sdl2:free-version! version)))


(test-group "sdl2:free-version!"
  (let ((version (sdl2:make-version)))
    (sdl2:free-version! version)
    (test-assert "sets the record's pointer to null"
                 (sdl2:struct-null? version)))

  (let ((version (sdl2:make-version)))
    (test-assert "returns the same instance"
                 (eq? version (sdl2:free-version! version)))
    (test-assert "is safe to use multiple times on the same version"
                 (eq? version (sdl2:free-version! version))))

  (test-error (sdl2:free-version! 0))
  (test-error (sdl2:free-version! #f))
  (test-error (sdl2:free-version! '(1 2 3)))
  (test-error (sdl2:free-version! (sdl2:make-point))))


(test-end "version")
