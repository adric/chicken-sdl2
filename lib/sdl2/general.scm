;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export init!
        init-subsystem!
        quit!
        quit-subsystem!
        was-init
        set-main-ready!

        clear-error!
        get-error
        set-error!

        get-platform

        disable-screen-saver!
        enable-screen-saver!
        screen-saver-enabled?

        has-clipboard-text?
        get-clipboard-text
        set-clipboard-text!

        get-version
        get-compiled-version
        version-at-least?)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION AND SHUTDOWN

(define (init! #!optional (flags '(everything)))
  (= 0 (SDL_Init (pack-init-flags flags))))

(define (init-subsystem! flags)
  (= 0 (SDL_InitSubSystem (pack-init-flags flags))))

(define (quit!)
  (SDL_Quit))

(define (quit-subsystem! flags)
  (SDL_QuitSubSystem (pack-init-flags flags)))

(define (was-init #!optional (flags '(everything)))
  (unpack-init-flags
   (SDL_WasInit (pack-init-flags flags))
   #t))

(define (set-main-ready!)
  (SDL_SetMainReady))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ERROR HANDLING

(define (clear-error!)
  (SDL_ClearError))

(define (get-error)
  (SDL_GetError))

(define (set-error! message)
  (SDL_SetError message))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLATFORM

(define (get-platform)
  (SDL_GetPlatform))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCREEN SAVER

(define (disable-screen-saver!)
  (SDL_DisableScreenSaver))

(define (enable-screen-saver!)
  (SDL_EnableScreenSaver))

(define (screen-saver-enabled?)
  (SDL_IsScreenSaverEnabled))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CLIP BOARD

(define (has-clipboard-text?)
  (SDL_HasClipboardText))

(define (get-clipboard-text)
  (SDL_GetClipboardText))

(define (set-clipboard-text! text)
  (SDL_SetClipboardText text))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERSION

(define (get-version)
  (let ((v (make-version)))
    (SDL_GetVersion v)
    v))

(define (get-compiled-version)
  (let ((v (make-version)))
    (SDL_VERSION v)
    v))

(define (version-at-least? x y z)
  (SDL_VERSION_ATLEAST x y z))
