;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013, 2015  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export version-major  version-major-set!
        version-minor  version-minor-set!
        version-patch  version-patch-set!
        make-version
        make-version*
        version-set!
        version->list
        version=?)


(define-struct-field-accessors
  SDL_version*
  version?
  ("major"
   type:   Uint8
   getter: version-major
   setter: version-major-set!
   guard:  (Uint8-guard "sdl2:version field major"))
  ("minor"
   type:   Uint8
   getter: version-minor
   setter: version-minor-set!
   guard:  (Uint8-guard "sdl2:version field minor"))
  ("patch"
   type:   Uint8
   getter: version-patch
   setter: version-patch-set!
   guard:  (Uint8-guard "sdl2:version field patch")))


(define (make-version #!optional (major 0) (minor 0) (patch 0))
  (version-set! (alloc-version) major minor patch))

(define (make-version* #!optional (major 0) (minor 0) (patch 0))
  (version-set! (alloc-version*) major minor patch))

(define (version-set! version #!optional major minor patch)
  (when major (version-major-set! version major))
  (when minor (version-minor-set! version minor))
  (when patch (version-patch-set! version patch))
  version)

(define (version->list version)
  (list (version-major version)
        (version-minor version)
        (version-patch version)))

(define (version=? version1 version2)
  (define foreign-equals
    (foreign-lambda*
     bool ((SDL_version* v1) (SDL_version* v2))
     "C_return(((v1->major == v2->major) &&
                (v1->minor == v2->minor) &&
                (v1->patch == v2->patch))
               ? 1 : 0);"))
  (foreign-equals version1 version2))
